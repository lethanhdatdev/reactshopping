import React from 'react';
import styled from 'styled-components';
import { HashRouter, Route, Switch } from 'react-router-dom';
import './scss/style.scss';

const Layout = React.lazy(() => import('./views/general/Layout'))
 
interface AppProps {
  name: string;
}

type ContainerProps = {
  padding?: string | 0;
  margin?: string | 0;
};

export const Container = styled.div<ContainerProps>`
  padding: ${props => ('padding' in props ? props.padding : '0')};
  margin: ${props => ('margin' in props ? props.margin : 0)};
`;

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)
export default function App({ name }: AppProps) {
  return (
    <HashRouter>
      <React.Suspense fallback={loading}>
        <Switch>
          <Route exact path="/" name="Home" render={(props: any) => <Layout {...props} />} />
        </Switch>
      </React.Suspense>
    </HashRouter>
    // <Container padding="1em">Hello {name}!</Container>
  );
}
